(function () {
'use strict';

const createRenderer = (getStore, settings) => {
	// ==========================
	// Set Canvas
	// ==========================
	const canvas = document.getElementById("canvas"),
		ctx = canvas.getContext("2d");

	// ========================
	// draw
	// ========================
	const draw = (i) => {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.font="30px Verdana";
		ctx.fillStyle = "#ffffff";
	
		// ====================
		// render bg
		// ====================
		if (settings.fynn) {
			getStore().bg.forEach(obj => {
				ctx.drawImage(
					settings.fynn,
					0 + (obj.type * 200), // source x
					200, // source y
					200, // source w
					200, // source h
					obj.x, // x
					obj.y, // y
					200, // w
					200 // h
				);
			});
		}

		// ====================
		// renderer player
		// ====================
		if (settings.fynn) {
			let j = i;
			if (!getStore().play) {
				j = 0;
			}

			ctx.drawImage(
				settings.fynn,
				0 + (((Math.floor(j % 8) >= 4)?0:1) * 200), // source x
				0, // source y
				200, // source w
				200, // source h
				settings.playerX - (settings.boundingBoxPlayer.w / 2), // x
				Math.floor(300 - getStore().jump), // y
				200, // w
				200 // h
			);
		}

		// ====================
		// render objs
		// ====================
		getStore().objs.forEach(obj => {
			const img = (getStore().play) ? obj.img : ((Math.floor(i % 8) >= 4)?2:3);
			ctx.drawImage(
				settings.fynn,
				0 + (img * 200), // source x
				0, // source y
				200, // source w
				200, // source h
				obj.x - (settings.boundingBoxObj.w * 2.2), // x
				obj.y - 130, // y
				200, // w
				200 // h
			);
		});

		ctx.textAlign="center"; 
		ctx.fillText(Math.floor(getStore().points), canvas.width / 2, 40);

		if (!getStore().play) {
			ctx.textAlign="center"; 
			ctx.fillText("Drücke zum starten", Math.floor(settings.width / 2), Math.floor(settings.height / 6));			
		}
	};

	// ========================
	// resize
	// ========================
	const resize = (width, height) => {
		settings = Object.assign({}, settings, {
			width,
			height
		});

		canvas.width = settings.width;
		//canvas.height = settings.height;
		canvas.height = 500;

		draw();
	};

	// ========================
	// fynn image
	// ========================
	const setFynn = (img) => {
		settings.fynn = img;
	};

	return { resize, draw, setFynn };
};

// renderer
// settings
const settings = {
	up: 15, // speed hoch
	down: 10, // speed down
	speed: 10, // speed objs
	maxJump: 250,
	boundingBoxPlayer: {
		w: 100,
		h: 200
	},
	boundingBoxObj: {
		w: 40,
		h: 60
	},
	playerX: 100,
	groundY: 400
};

// last
let nextObjTimer = null;
let nextBGTimer = null;
let speedTimer = null;

// store
let store = {
	jump: 0,
	vel: 0,
	objs: [],
	bg: [],
	play: true,
	points: 0,
	end: 0
};

const getStore = () => {
	return store;
};

// ==========================
// init Renderer
// ==========================
const renderer = createRenderer(getStore, settings);

// ==========================
// Fallback
// ==========================
window.requestAnimFrame = (function() {
	return (
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		}
	);
})();

// ==========================
// set size
// ==========================
renderer.resize(window.innerWidth, window.innerHeight - 18);

// ==========================
// Resize
// ==========================
window.onresize = function() {
	var width = window.innerWidth,
		height = window.innerHeight;

	renderer.resize(width, height);
};

// ==========================
// Restart
// ==========================
const restart = () => {
	console.log("start");
	store = {
		jump: 0,
		vel: 0,
		objs: [],
		bg: [],
		play: true,
		points: 0,
		end: 0
	};

	settings.speed = 10;

	timerBG();
	objBG();
	setSpeedTimer();
};

// ==========================
// Panik Modus Start
// ==========================
const panik = () => {
	store.objs.forEach((obj) => {
		obj.i = getRandomInt(0, 1000);
		obj.s = getRandomInt(settings.speed * 0.5, settings.speed * 1.5);
	});
};

// ==========================
// Timeout Function BG
// ==========================
const setSpeedTimer = () => {
	if (store.play) {
		settings.speed += 5;

		speedTimer = setTimeout(setSpeedTimer, 30 * 1000);
	}
};

// ==========================
// Timeout Function BG
// ==========================
const timerBG = () => {
	if (store.play) {
		store.bg.push({
			x: window.innerWidth,
			y: settings.groundY - 200 - getRandomInt(0,100),
			type: (getRandomInt(0,5) == 5) ? getRandomInt(0,5) : getRandomInt(0,2)
		});

		store.bg = store.bg.sort(function(a, b) {
		    return parseFloat(a.y) - parseFloat(b.y);
		});

		nextBGTimer = setTimeout(timerBG, settings.speed * 10 + getRandomInt(500, 1000));
	}
};

// ==========================
// Timeout Function OBJ
// ==========================
const objBG = () => {
	if (store.play) {
		store.objs.push({
			x: window.innerWidth,
			y: settings.groundY,
			i: 0,
			img: getRandomInt(2, 3)
		});

		nextObjTimer = setTimeout(objBG, settings.speed * 10 + getRandomInt(500, 2000));
	}
};

// ==========================
// Update
// ==========================
const playerRight = settings.playerX + settings.boundingBoxPlayer.w;

const update = timer => {
	// ======================
	// move player
	// ======================
	if (store.play && store.jump > settings.maxJump && store.vel > 0) {
		store.vel = -settings.down;
	}

	store.jump = Math.max(0, store.jump + store.vel);

	// ======================
	// move objs
	// ======================
	store.objs = store.objs
		.map(obj => {
			obj.x = obj.x - ((store.play) ? settings.speed : -obj.s);
			if (!store.play) {
				obj.i++;
				obj.y = obj.y + ((obj.i % 60) > 30 ? -1 : 1);
			}
			return obj;
		})
		.filter(obj => {
			return obj.x >= -200;
		});

	if (!store.play) {
		return
	}

	// ======================
	// move bg
	// ======================
	store.bg = store.bg
		.map(obj => {
			obj.x = obj.x - settings.speed;
			return obj;
		})
		.filter(obj => {
			return obj.x >= -200;
		});

	// ======================
	// collision detection
	// ======================
	const y = Math.floor(300 - store.jump);
	const playerBottom = y + settings.boundingBoxPlayer.h;

	for (var i = 0; i < store.objs.length; i++) {
		const obj = store.objs[i];

		if (
			obj.x > settings.playerX &&
			obj.x < playerRight &&
			obj.y >= y &&
			obj.y <= playerBottom
		) {
			console.log("collision detected!");
			store.play = false;
			store.end = Date.now();
			clearTimeout(nextBGTimer);
			clearTimeout(nextObjTimer);
			clearTimeout(speedTimer);

			return panik()
		}
	}
//	store.objs.forEach(obj => {
//	});

	if (store.play) {
		store.points += 0.02;
	}
};

// ==========================
// Key Event
// ==========================
const action = (event) => {
	if (store.play) {
		if (store.jump == 0) {
			store.vel = settings.up;
		}
	} else if (store.end + 1000 < Date.now()) {
		restart();
	}
};
document.addEventListener('keydown', action, false);
document.body.addEventListener('touchstart', action, false);

// ==========================
// Loop
// ==========================
const loop = () => {
	window.requestAnimFrame(i => {
		update(i);
		renderer.draw(i);
		loop();
	});
};

// ==========================
// Grafik Preload
// ==========================
const fynn = new Image();
fynn.src = "media/img/fynn.png";
fynn.onload = () => {
	renderer.setFynn(fynn);
	timerBG();
	objBG();
	setSpeedTimer();
	loop();
};

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

}());
