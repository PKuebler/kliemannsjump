var gulp = require("gulp-param")(require("gulp"), process.argv),
	// watch and serve
	connect = require("gulp-connect"),
	watch = require("gulp-watch"),
	// upload
	sftp = require("gulp-sftp"),
	// css
	less = require("gulp-less"),
	autoprefixer = require("gulp-autoprefixer"),
	uncss = require("gulp-uncss"),
	minifyCss = require("gulp-minify-css"),
	// js
	rollup = require("rollup-stream"),
	source = require("vinyl-source-stream"),
	// images
	image = require("gulp-image"),
	// content
	twig = require("gulp-twig");

// Content
// ==============================================
gulp.task("content", function() {
	return gulp
		.src("./app/views/index.twig")
		.pipe(twig({}))
		.pipe(gulp.dest("./public"))
		.pipe(connect.reload());
});

// Less
// ==============================================
gulp.task("css", function() {
	return gulp
		.src("./app/less/style.less")
		.pipe(less({ compress: true }))
		.pipe(
			autoprefixer({
				browsers: ["last 2 versions"],
				cascade: false
			})
		)
		.pipe(
			uncss({
				html: ["app/views/**/*.twig"],
				ignore: []
			})
		)
		.pipe(minifyCss({ keepBreaks: false }))
		.pipe(gulp.dest("./public/media/css"))
		.pipe(connect.reload());
});

// Rollup / JS
// ==============================================
gulp.task("js", function() {
	return rollup({
			input: "./app/js/main.js",
			format: "iife"
		})
		.pipe(source("app.js"))
		.pipe(gulp.dest("./public/media/js"))
		.pipe(connect.reload());
});

// Images
// ==============================================
gulp.task("images", function() {
	return gulp
		.src("./app/images/**/*")
		.pipe(
			image({
				pngquant: true,
				optipng: true,
				zopflipng: true,
				advpng: true,
				jpegRecompress: true,
				jpegoptim: true,
				mozjpeg: true,
				gifsicle: true,
				svgo: true
			})
		)
		.pipe(gulp.dest("./public/media/img"))
		.pipe(connect.reload());
});

// Watch
// ==============================================
gulp.task("watch", function() {
	watch("./app/less/**/*.less", function(event) {
		gulp.start("css");
	});
	watch("./app/views/**/*.twig", function(event) {
		gulp.start("content");
	});
	watch("./app/js/**/*", function(event) {
		gulp.start("js");
	});
	watch("./app/images/**/*", function(event) {
		gulp.start("images");
	});
});

// Preview
// ==============================================
gulp.task("connect", function() {
	connect.server({
		root: "./public",
		livereload: true
	});
});

// Upload
// ==============================================
gulp.task("publish", function(host, user, pass) {
	return gulp.src("./public/**/*").pipe(
		sftp({
			host: host,
			user: user,
			pass: pass,
			port: 2022,
			remotePath: "/data/" + host + "/"
		})
	);
});

// Pipelines
// ==============================================
gulp.task("default", ["build", "connect", "watch"]);
gulp.task("build", ["css", "content", "images", "js"]);
