// renderer
import createRenderer from "./draw/renderer.js";

// settings
const settings = {
	up: 15, // speed hoch
	down: 10, // speed down
	speed: 10, // speed objs
	maxJump: 250,
	boundingBoxPlayer: {
		w: 100,
		h: 200
	},
	boundingBoxObj: {
		w: 40,
		h: 60
	},
	playerX: 100,
	groundY: 400
};

// gfx
const gfx = {};

// last
let nextObjTimer = null;
let nextBGTimer = null;
let speedTimer = null;

// store
let store = {
	jump: 0,
	vel: 0,
	objs: [],
	bg: [],
	play: true,
	points: 0,
	end: 0
};

const getStore = () => {
	return store;
};

// ==========================
// init Renderer
// ==========================
const renderer = createRenderer(getStore, settings);

// ==========================
// Fallback
// ==========================
window.requestAnimFrame = (function() {
	return (
		window.requestAnimationFrame ||
		window.webkitRequestAnimationFrame ||
		window.mozRequestAnimationFrame ||
		window.oRequestAnimationFrame ||
		window.msRequestAnimationFrame ||
		function(callback) {
			window.setTimeout(callback, 1000 / 60);
		}
	);
})();

// ==========================
// set size
// ==========================
renderer.resize(window.innerWidth, window.innerHeight - 18);

// ==========================
// Resize
// ==========================
window.onresize = function() {
	var width = window.innerWidth,
		height = window.innerHeight;

	renderer.resize(width, height);
};

// ==========================
// Restart
// ==========================
const restart = () => {
	console.log("start");
	store = {
		jump: 0,
		vel: 0,
		objs: [],
		bg: [],
		play: true,
		points: 0,
		end: 0
	};

	settings.speed = 10;

	timerBG();
	objBG();
	setSpeedTimer();
}

// ==========================
// Panik Modus Start
// ==========================
const panik = () => {
	store.objs.forEach((obj) => {
		obj.i = getRandomInt(0, 1000)
		obj.s = getRandomInt(settings.speed * 0.5, settings.speed * 1.5)
	})
}

// ==========================
// Timeout Function BG
// ==========================
const setSpeedTimer = () => {
	if (store.play) {
		settings.speed += 5;

		speedTimer = setTimeout(setSpeedTimer, 30 * 1000)
	}
}

// ==========================
// Timeout Function BG
// ==========================
const timerBG = () => {
	if (store.play) {
		store.bg.push({
			x: window.innerWidth,
			y: settings.groundY - 200 - getRandomInt(0,100),
			type: (getRandomInt(0,5) == 5) ? getRandomInt(0,5) : getRandomInt(0,2)
		});

		store.bg = store.bg.sort(function(a, b) {
		    return parseFloat(a.y) - parseFloat(b.y);
		})

		nextBGTimer = setTimeout(timerBG, Math.max(0, 100 - settings.speed) + getRandomInt(500, 1000))
	}
}

// ==========================
// Timeout Function OBJ
// ==========================
const objBG = () => {
	if (store.play) {
		store.objs.push({
			x: window.innerWidth,
			y: settings.groundY,
			i: 0,
			img: getRandomInt(2, 3)
		});

		nextObjTimer = setTimeout(objBG, Math.max(0, 100 - settings.speed) + getRandomInt(500, 2000))
	}
}

// ==========================
// Update
// ==========================
const playerRight = settings.playerX + settings.boundingBoxPlayer.w

const update = timer => {
	// ======================
	// move player
	// ======================
	if (store.play && store.jump > settings.maxJump && store.vel > 0) {
		store.vel = -settings.down;
	}

	store.jump = Math.max(0, store.jump + store.vel);

	// ======================
	// move objs
	// ======================
	store.objs = store.objs
		.map(obj => {
			obj.x = obj.x - ((store.play) ? settings.speed : -obj.s);
			if (!store.play) {
				obj.i++
				obj.y = obj.y + ((obj.i % 60) > 30 ? -1 : 1)
			}
			return obj;
		})
		.filter(obj => {
			return obj.x >= -200;
		});

	if (!store.play) {
		return
	}

	// ======================
	// move bg
	// ======================
	store.bg = store.bg
		.map(obj => {
			obj.x = obj.x - settings.speed;
			return obj;
		})
		.filter(obj => {
			return obj.x >= -200;
		});

	// ======================
	// collision detection
	// ======================
	const y = Math.floor(300 - store.jump);
	const playerBottom = y + settings.boundingBoxPlayer.h

	for (var i = 0; i < store.objs.length; i++) {
		const obj = store.objs[i]

		if (
			obj.x > settings.playerX &&
			obj.x < playerRight &&
			obj.y >= y &&
			obj.y <= playerBottom
		) {
			console.log("collision detected!");
			store.play = false;
			store.end = Date.now();
			clearTimeout(nextBGTimer);
			clearTimeout(nextObjTimer);
			clearTimeout(speedTimer);

			return panik()
		}
	}
//	store.objs.forEach(obj => {
//	});

	if (store.play) {
		store.points += 0.02;
	}
};

// ==========================
// Key Event
// ==========================
const action = (event) => {
	if (store.play) {
		if (store.jump == 0) {
			store.vel = settings.up;
		}
	} else if (store.end + 1000 < Date.now()) {
		restart();
	}
};
document.addEventListener('keydown', action, false);
document.body.addEventListener('touchstart', action, false);

// ==========================
// Loop
// ==========================
const loop = () => {
	window.requestAnimFrame(i => {
		update(i);
		renderer.draw(i);
		loop();
	});
};

// ==========================
// Grafik Preload
// ==========================
const fynn = new Image();
fynn.src = "media/img/fynn.png"
fynn.onload = () => {
	renderer.setFynn(fynn);
	timerBG();
	objBG();
	setSpeedTimer()
	loop();
}

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
