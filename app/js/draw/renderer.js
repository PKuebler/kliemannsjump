const createRenderer = (getStore, settings) => {
	// ==========================
	// Set Canvas
	// ==========================
	const canvas = document.getElementById("canvas"),
		ctx = canvas.getContext("2d");

	// ========================
	// draw
	// ========================
	const draw = (i) => {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.font="30px Verdana";
		ctx.fillStyle = "#ffffff";
	
		// ====================
		// render bg
		// ====================
		if (settings.fynn) {
			getStore().bg.forEach(obj => {
				ctx.drawImage(
					settings.fynn,
					0 + (obj.type * 200), // source x
					200, // source y
					200, // source w
					200, // source h
					obj.x, // x
					obj.y, // y
					200, // w
					200 // h
				);
			});
		}

		// ====================
		// renderer player
		// ====================
		if (settings.fynn) {
			let j = i
			if (!getStore().play) {
				j = 0;
			}

			const runSpeed = 8;

			ctx.drawImage(
				settings.fynn,
				0 + (((Math.floor(j % 8) >= 4)?0:1) * 200), // source x
				0, // source y
				200, // source w
				200, // source h
				settings.playerX - (settings.boundingBoxPlayer.w / 2), // x
				Math.floor(300 - getStore().jump), // y
				200, // w
				200 // h
			);
		}

		// ====================
		// render objs
		// ====================
		getStore().objs.forEach(obj => {
			const img = (getStore().play) ? obj.img : ((Math.floor(i % 8) >= 4)?2:3)
			ctx.drawImage(
				settings.fynn,
				0 + (img * 200), // source x
				0, // source y
				200, // source w
				200, // source h
				obj.x - (settings.boundingBoxObj.w * 2.2), // x
				obj.y - 130, // y
				200, // w
				200 // h
			);
		});

		ctx.textAlign="center"; 
		ctx.fillText(Math.floor(getStore().points), canvas.width / 2, 40);

		if (!getStore().play) {
			ctx.textAlign="center"; 
			ctx.fillText("Drücke zum starten", Math.floor(settings.width / 2), Math.floor(settings.height / 6));			
		}
	};

	// ========================
	// resize
	// ========================
	const resize = (width, height) => {
		settings = Object.assign({}, settings, {
			width,
			height
		});

		canvas.width = settings.width;
		//canvas.height = settings.height;
		canvas.height = 500;

		draw();
	};

	// ========================
	// fynn image
	// ========================
	const setFynn = (img) => {
		settings.fynn = img;
	};

	return { resize, draw, setFynn };
};

export default createRenderer;
